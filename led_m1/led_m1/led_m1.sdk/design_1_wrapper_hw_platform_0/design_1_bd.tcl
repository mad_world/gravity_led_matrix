
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2015.4
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   puts "ERROR: This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If you do not already have a project created,
# you can create a project using the following command:
#    create_project project_1 myproj -part xc7z020clg484-1
#    set_property BOARD_PART em.avnet.com:zed:part0:1.3 [current_project]

# CHECKING IF PROJECT EXISTS
if { [get_projects -quiet] eq "" } {
   puts "ERROR: Please open or create a project!"
   return 1
}



# CHANGE DESIGN NAME HERE
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "ERROR: Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      puts "INFO: Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   puts "INFO: Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   puts "INFO: Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   puts "INFO: Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

puts "INFO: Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   puts $errMsg
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]
  set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]
  set GPIO [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 GPIO ]
  set GPIO_1 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 GPIO_1 ]
  set spi_clk_s [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 spi_clk_s ]
  set spi_miso_s [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 spi_miso_s ]
  set spi_mosi_s [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 spi_mosi_s ]

  # Create ports
  set B1_S [ create_bd_port -dir O B1_S ]
  set B2_S [ create_bd_port -dir O B2_S ]
  set G1_S [ create_bd_port -dir O G1_S ]
  set G2_S [ create_bd_port -dir O G2_S ]
  set OE_S [ create_bd_port -dir O OE_S ]
  set R1_S [ create_bd_port -dir O R1_S ]
  set R2_S [ create_bd_port -dir O R2_S ]
  set ROW_Date_S [ create_bd_port -dir O -from 4 -to 0 ROW_Date_S ]
  set SH_S [ create_bd_port -dir O SH_S ]
  set ST_S [ create_bd_port -dir O ST_S ]

  # Create instance: axi_gpio_0, and set properties
  set axi_gpio_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0 ]
  set_property -dict [ list \
CONFIG.C_ALL_OUTPUTS {1} \
CONFIG.C_GPIO_WIDTH {1} \
 ] $axi_gpio_0

  # Create instance: led_m1_ip_0, and set properties
  set led_m1_ip_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:led_m1_ip:1.0 led_m1_ip_0 ]

  # Create instance: processing_system7_0, and set properties
  set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
  set_property -dict [ list \
CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {0} \
CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {1} \
CONFIG.PCW_SD0_PERIPHERAL_ENABLE {0} \
CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {0} \
CONFIG.PCW_USB0_PERIPHERAL_ENABLE {0} \
CONFIG.preset {ZedBoard} \
 ] $processing_system7_0

  # Create instance: processing_system7_0_axi_periph, and set properties
  set processing_system7_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 processing_system7_0_axi_periph ]
  set_property -dict [ list \
CONFIG.NUM_MI {5} \
 ] $processing_system7_0_axi_periph

  # Create instance: rst_processing_system7_0_50M, and set properties
  set rst_processing_system7_0_50M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_processing_system7_0_50M ]

  # Create instance: spi_clk, and set properties
  set spi_clk [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 spi_clk ]
  set_property -dict [ list \
CONFIG.C_ALL_OUTPUTS {1} \
CONFIG.C_GPIO_WIDTH {1} \
 ] $spi_clk

  # Create instance: spi_miso, and set properties
  set spi_miso [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 spi_miso ]
  set_property -dict [ list \
CONFIG.C_ALL_INPUTS {1} \
CONFIG.C_GPIO_WIDTH {1} \
 ] $spi_miso

  # Create instance: spi_mosi, and set properties
  set spi_mosi [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 spi_mosi ]
  set_property -dict [ list \
CONFIG.C_ALL_OUTPUTS {1} \
CONFIG.C_GPIO_WIDTH {1} \
 ] $spi_mosi

  # Create interface connections
  connect_bd_intf_net -intf_net axi_gpio_0_GPIO [get_bd_intf_ports GPIO_1] [get_bd_intf_pins axi_gpio_0/GPIO]
  connect_bd_intf_net -intf_net processing_system7_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins processing_system7_0/DDR]
  connect_bd_intf_net -intf_net processing_system7_0_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins processing_system7_0/FIXED_IO]
  connect_bd_intf_net -intf_net processing_system7_0_M_AXI_GP0 [get_bd_intf_pins processing_system7_0/M_AXI_GP0] [get_bd_intf_pins processing_system7_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_axi_periph_M00_AXI [get_bd_intf_pins led_m1_ip_0/S00_AXI] [get_bd_intf_pins processing_system7_0_axi_periph/M00_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_axi_periph_M01_AXI [get_bd_intf_pins processing_system7_0_axi_periph/M01_AXI] [get_bd_intf_pins spi_clk/S_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_axi_periph_M02_AXI [get_bd_intf_pins processing_system7_0_axi_periph/M02_AXI] [get_bd_intf_pins spi_mosi/S_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_axi_periph_M03_AXI [get_bd_intf_pins processing_system7_0_axi_periph/M03_AXI] [get_bd_intf_pins spi_miso/S_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_axi_periph_M04_AXI [get_bd_intf_pins axi_gpio_0/S_AXI] [get_bd_intf_pins processing_system7_0_axi_periph/M04_AXI]
  connect_bd_intf_net -intf_net spi_clk_GPIO [get_bd_intf_ports spi_clk_s] [get_bd_intf_pins spi_clk/GPIO]
  connect_bd_intf_net -intf_net spi_miso_GPIO [get_bd_intf_ports spi_miso_s] [get_bd_intf_pins spi_miso/GPIO]
  connect_bd_intf_net -intf_net spi_mosi_GPIO [get_bd_intf_ports spi_mosi_s] [get_bd_intf_pins spi_mosi/GPIO]

  # Create port connections
  connect_bd_net -net led_m1_ip_0_B1_S [get_bd_ports B1_S] [get_bd_pins led_m1_ip_0/B1_S]
  connect_bd_net -net led_m1_ip_0_B2_S [get_bd_ports B2_S] [get_bd_pins led_m1_ip_0/B2_S]
  connect_bd_net -net led_m1_ip_0_G1_S [get_bd_ports G1_S] [get_bd_pins led_m1_ip_0/G1_S]
  connect_bd_net -net led_m1_ip_0_G2_S [get_bd_ports G2_S] [get_bd_pins led_m1_ip_0/G2_S]
  connect_bd_net -net led_m1_ip_0_OE_S [get_bd_ports OE_S] [get_bd_pins led_m1_ip_0/OE_S]
  connect_bd_net -net led_m1_ip_0_R1_S [get_bd_ports R1_S] [get_bd_pins led_m1_ip_0/R1_S]
  connect_bd_net -net led_m1_ip_0_R2_S [get_bd_ports R2_S] [get_bd_pins led_m1_ip_0/R2_S]
  connect_bd_net -net led_m1_ip_0_ROW_Date_S [get_bd_ports ROW_Date_S] [get_bd_pins led_m1_ip_0/ROW_Date_S]
  connect_bd_net -net led_m1_ip_0_SH_S [get_bd_ports SH_S] [get_bd_pins led_m1_ip_0/SH_S]
  connect_bd_net -net led_m1_ip_0_ST_S [get_bd_ports ST_S] [get_bd_pins led_m1_ip_0/ST_S]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins axi_gpio_0/s_axi_aclk] [get_bd_pins led_m1_ip_0/s00_axi_aclk] [get_bd_pins processing_system7_0/FCLK_CLK0] [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins processing_system7_0_axi_periph/ACLK] [get_bd_pins processing_system7_0_axi_periph/M00_ACLK] [get_bd_pins processing_system7_0_axi_periph/M01_ACLK] [get_bd_pins processing_system7_0_axi_periph/M02_ACLK] [get_bd_pins processing_system7_0_axi_periph/M03_ACLK] [get_bd_pins processing_system7_0_axi_periph/M04_ACLK] [get_bd_pins processing_system7_0_axi_periph/S00_ACLK] [get_bd_pins rst_processing_system7_0_50M/slowest_sync_clk] [get_bd_pins spi_clk/s_axi_aclk] [get_bd_pins spi_miso/s_axi_aclk] [get_bd_pins spi_mosi/s_axi_aclk]
  connect_bd_net -net processing_system7_0_FCLK_RESET0_N [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins rst_processing_system7_0_50M/ext_reset_in]
  connect_bd_net -net rst_processing_system7_0_50M_interconnect_aresetn [get_bd_pins processing_system7_0_axi_periph/ARESETN] [get_bd_pins rst_processing_system7_0_50M/interconnect_aresetn]
  connect_bd_net -net rst_processing_system7_0_50M_peripheral_aresetn [get_bd_pins axi_gpio_0/s_axi_aresetn] [get_bd_pins led_m1_ip_0/s00_axi_aresetn] [get_bd_pins processing_system7_0_axi_periph/M00_ARESETN] [get_bd_pins processing_system7_0_axi_periph/M01_ARESETN] [get_bd_pins processing_system7_0_axi_periph/M02_ARESETN] [get_bd_pins processing_system7_0_axi_periph/M03_ARESETN] [get_bd_pins processing_system7_0_axi_periph/M04_ARESETN] [get_bd_pins processing_system7_0_axi_periph/S00_ARESETN] [get_bd_pins rst_processing_system7_0_50M/peripheral_aresetn] [get_bd_pins spi_clk/s_axi_aresetn] [get_bd_pins spi_miso/s_axi_aresetn] [get_bd_pins spi_mosi/s_axi_aresetn]

  # Create address segments
  create_bd_addr_seg -range 0x10000 -offset 0x41230000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_gpio_0/S_AXI/Reg] SEG_axi_gpio_0_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x43C00000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs led_m1_ip_0/S00_AXI/S00_AXI_reg] SEG_led_m1_ip_0_S00_AXI_reg
  create_bd_addr_seg -range 0x10000 -offset 0x41200000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs spi_clk/S_AXI/Reg] SEG_spi_clk_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x41220000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs spi_miso/S_AXI/Reg] SEG_spi_miso_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x41210000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs spi_mosi/S_AXI/Reg] SEG_spi_mosi_Reg

  # Perform GUI Layout
  regenerate_bd_layout -layout_string {
   guistr: "# # String gsaved with Nlview 6.5.5  2015-06-26 bk=1.3371 VDI=38 GEI=35 GUI=JA:1.6
#  -string -flagsOSRD
preplace port DDR -pg 1 -y 40 -defaultsOSRD
preplace port spi_clk_s -pg 1 -y 130 -defaultsOSRD
preplace port spi_cs -pg 1 -y 480 -defaultsOSRD
preplace port OE_S -pg 1 -y 630 -defaultsOSRD
preplace port spi_miso_s -pg 1 -y 250 -defaultsOSRD
preplace port B2_S -pg 1 -y 570 -defaultsOSRD
preplace port B1_S -pg 1 -y 510 -defaultsOSRD
preplace port R1_S -pg 1 -y 410 -defaultsOSRD
preplace port SH_S -pg 1 -y 610 -defaultsOSRD
preplace port G2_S -pg 1 -y 530 -defaultsOSRD
preplace port FIXED_IO -pg 1 -y 60 -defaultsOSRD
preplace port ST_S -pg 1 -y 590 -defaultsOSRD
preplace port spi_mosi_s -pg 1 -y 370 -defaultsOSRD
preplace port R2_S -pg 1 -y 550 -defaultsOSRD
preplace port G1_S -pg 1 -y 430 -defaultsOSRD
preplace portBus ROW_Date_S -pg 1 -y 450 -defaultsOSRD
preplace inst axi_gpio_0 -pg 1 -lvl 2 -y 480 -defaultsOSRD
preplace inst rst_processing_system7_0_50M -pg 1 -lvl 1 -y 260 -defaultsOSRD
preplace inst spi_clk -pg 1 -lvl 3 -y 50 -defaultsOSRD
preplace inst spi_miso -pg 1 -lvl 3 -y 170 -defaultsOSRD
preplace inst spi_mosi -pg 1 -lvl 3 -y 290 -defaultsOSRD
preplace inst led_m1_ip_0 -pg 1 -lvl 3 -y 540 -defaultsOSRD
preplace inst processing_system7_0_axi_periph -pg 1 -lvl 2 -y 190 -defaultsOSRD
preplace inst processing_system7_0 -pg 1 -lvl 1 -y 80 -defaultsOSRD
preplace netloc processing_system7_0_DDR 1 1 3 NJ -20 NJ -20 NJ
preplace netloc led_m1_ip_0_ST_S 1 3 1 N
preplace netloc processing_system7_0_axi_periph_M00_AXI 1 2 1 700
preplace netloc processing_system7_0_axi_periph_M03_AXI 1 2 1 710
preplace netloc rst_processing_system7_0_50M_interconnect_aresetn 1 1 1 380
preplace netloc processing_system7_0_M_AXI_GP0 1 1 1 360
preplace netloc led_m1_ip_0_R1_S 1 3 1 1220
preplace netloc led_m1_ip_0_OE_S 1 3 1 N
preplace netloc led_m1_ip_0_SH_S 1 3 1 N
preplace netloc processing_system7_0_FCLK_RESET0_N 1 0 2 -10 -10 330
preplace netloc rst_processing_system7_0_50M_peripheral_aresetn 1 1 2 370 -10 690
preplace netloc processing_system7_0_axi_periph_M02_AXI 1 2 1 680
preplace netloc led_m1_ip_0_G1_S 1 3 1 1230
preplace netloc led_m1_ip_0_R2_S 1 3 1 N
preplace netloc led_m1_ip_0_ROW_Date_S 1 3 1 N
preplace netloc spi_mosi_GPIO 1 3 1 NJ
preplace netloc processing_system7_0_FIXED_IO 1 1 3 NJ 390 NJ 380 NJ
preplace netloc axi_gpio_0_GPIO 1 2 2 NJ 400 NJ
preplace netloc spi_clk_GPIO 1 3 1 NJ
preplace netloc led_m1_ip_0_B2_S 1 3 1 N
preplace netloc processing_system7_0_FCLK_CLK0 1 0 3 -20 -20 350 400 720
preplace netloc processing_system7_0_axi_periph_M04_AXI 1 1 2 380 410 670
preplace netloc spi_miso_GPIO 1 3 1 NJ
preplace netloc processing_system7_0_axi_periph_M01_AXI 1 2 1 680
preplace netloc led_m1_ip_0_G2_S 1 3 1 N
preplace netloc led_m1_ip_0_B1_S 1 3 1 N
levelinfo -pg 1 -40 160 530 1080 1270 -top -30 -bot 750
",
}

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


