/*
 * ball.h
 *
 *  Created on: 2018年1月25日
 *      Author: madworld
 */

#ifndef BALL_H_
#define BALL_H_
#include <math.h>
#include <xbasic_types.h>
#define  R 0.0015f
#define  XN 64*2*R
#define  YN 64*2*R                   // 设置图形窗口的大小
#define  N  20                      // 设置球的个数，最多 90 个
#define  V  20                      // 设置球的速度

int PerfectElasticCollision(float v1[2], float v2[2], float u[2]);
int BallMove(float x[], float y[], float a[], float b[], int n);
void Transfer(float x[], float y[], int a[], int b[], int n);
void Judge(float x[], float y[], float a[], float b[], int n);
#endif /* BALL_H_ */
