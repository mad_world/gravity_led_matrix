/*
 * mpu9250.c
 *
 *  Created on: 2018��1��26��
 *      Author: madworld
 */

#include "mpu9250.h"

signed short mpu_acce[3];
signed short mpu_gyro[3];
signed short mpu_magn[3];
signed short mpu_temp;

MpuCorrection mpu_correction;
void nop(void)
{
	int n=500;
	while(n--);
}
unsigned char spi_send(unsigned char dat)
{
	unsigned char i;
	unsigned char rcv = 0;

	for(i = 0; i < 8; i++)
	{
		SPI_SCL_L();

		if(dat & 0x80)
			SPI_SDI_H();
		else
			SPI_SDI_L();
		dat <<= 1;

		SPI_SCL_H();

		rcv <<= 1;
		if(SPI_SDO_READ())
			rcv++;

	}

	return rcv;

}


const unsigned char MPU_INIT_REG[][2] = {
	{MPU6500_PWR_MGMT_1,		0x80},		// Reset Device
	{0xFF, 20},								//

	{MPU6500_PWR_MGMT_1,		0x03},		// Clock Source
	{MPU6500_PWR_MGMT_2,		0x00},		// Enable Acc & Gyro
	{MPU6500_SMPLRT_DIV,		0x00},
	{MPU6500_CONFIG,			MPU_GYRO_LPS_184HZ},
	{MPU6500_GYRO_CONFIG,		MPU_GYRO_FS_1000},
	{MPU6500_ACCEL_CONFIG,		MPU_ACCE_FS_8G},
	{MPU6500_ACCEL_CONFIG_2,	MPU_ACCE_LPS_460HZ},

	{MPU6500_INT_PIN_CFG,		0x30},		// Disable Interrupt
	{MPU6500_I2C_MST_CTRL,		0x4D},		// I2C Speed 400 kHz
	{MPU6500_USER_CTRL,			0x20},		// Enable AUX

	{MPU6500_I2C_SLV4_CTRL,		0x13},
	{MPU6500_I2C_MST_DELAY_CTRL,0x01},

	{MPU6500_I2C_SLV0_ADDR,		AK8963_I2C_ADDR},
	{MPU6500_I2C_SLV0_CTRL,		0x81},			// Enable slave0 Length=1

	{MPU6500_I2C_SLV0_REG,		AK8963_CNTL2},	// reg
	{MPU6500_I2C_SLV0_DO,		0x01},			// dat
	{0xFF, 50},

	{MPU6500_I2C_SLV0_REG,		AK8963_CNTL1},	// reg
	{MPU6500_I2C_SLV0_DO,		0x16},			// dat
	{0xFF, 10},

	{MPU6500_I2C_SLV0_ADDR,		0x80 | AK8963_I2C_ADDR},
	{MPU6500_I2C_SLV0_REG,		AK8963_HXL},
	{MPU6500_I2C_SLV0_CTRL,		0x87},			// Enable slave0 Length=6
};
void mpu_send_1byte(unsigned char addr, unsigned char dat)
{
SPI_NCS_L();
	spi_send(addr);
	spi_send(dat);
SPI_NCS_H();
}

void mpu_read_nbyte(unsigned char addr, unsigned char *buf, unsigned char len)
{
	unsigned char i;

SPI_NCS_L();
	spi_send(0x80 | addr);
	for(i = 0; i < len; i++)
	{
		buf[i] = spi_send(0xFF);
		nop();
	}
SPI_NCS_H();
}

void mpu_init_correction()
{
		mpu_correction.gyr_offset[0] = 0;
		mpu_correction.gyr_offset[1] = 0;
		mpu_correction.gyr_offset[2] = 0;

		mpu_correction.gyr_gain[0] = MPU_GYRO_K * 1;
		mpu_correction.gyr_gain[1] = MPU_GYRO_K * 1;
		mpu_correction.gyr_gain[2] = MPU_GYRO_K * 1;

		mpu_correction.mag_offset[0] = 0;
		mpu_correction.mag_offset[1] = 0;
		mpu_correction.mag_offset[2] = 0;

		mpu_correction.mag_gain[0] = MPU_MAGN_K ;
		mpu_correction.mag_gain[1] = MPU_MAGN_K ;
		mpu_correction.mag_gain[2] = MPU_MAGN_K ;

		mpu_correction.acc_offset[0] = 0;
		mpu_correction.acc_offset[1] = 0;
		mpu_correction.acc_offset[2] = 0;

		mpu_correction.acc_gain[0] = MPU_ACCE_K;
		mpu_correction.acc_gain[1] = MPU_ACCE_K;
		mpu_correction.acc_gain[2] = MPU_ACCE_K;

	}

unsigned char mpu_read_1byte(unsigned char addr)
{
	unsigned char dat;

SPI_NCS_L();
	spi_send(0x80 | addr);
	dat = spi_send(0xFF);
	SPI_NCS_H();
	  printf("\r\n   DAT=%D.\r\n",dat);
	return dat;
}
void delay_ms(int ms)
{
	int m=0xfffff;
	while(ms--)
	{
		m=0x2ffff;
		while(m--);
	}
}

void mpu_read(MPU_READ_TypeDef mr)
{
	unsigned char dat[21];
	if(mr == MPU_READ_ALL)
	{
		mpu_read_nbyte(MPU6500_ACCEL_XOUT_H, dat, 21);
		mpu_acce[0] = (dat[0] << 8) + dat[1];
		mpu_acce[1] = (dat[2] << 8) + dat[3];
		mpu_acce[2] = (dat[4] << 8) + dat[5];
		mpu_temp = (dat[6] << 8) + dat[7];
		mpu_gyro[0] = (dat[8] << 8) + dat[9];
		mpu_gyro[1] = (dat[10] << 8) + dat[11];
		mpu_gyro[2] = (dat[12] << 8) + dat[13];
		if(dat[20] == 0x10)
		{
			mpu_magn[1] = (dat[15] << 8) + dat[14];
			mpu_magn[0] = (dat[17] << 8) + dat[16];
			mpu_magn[2] = (dat[19] << 8) + dat[18];
		}
	}
	else if(mr == (MPU_READ_GYRO | MPU_READ_ACCE | MPU_READ_TEMP))
	{
		mpu_read_nbyte(MPU6500_ACCEL_XOUT_H, dat, 14);
		mpu_acce[0] = (dat[0] << 8) + dat[1];
		mpu_acce[1] = (dat[2] << 8) + dat[3];
		mpu_acce[2] = (dat[4] << 8) + dat[5];
		mpu_temp = (dat[6] << 8) + dat[7];
		mpu_gyro[0] = (dat[8] << 8) + dat[9];
		mpu_gyro[1] = (dat[10] << 8) + dat[11];
		mpu_gyro[2] = (dat[12] << 8) + dat[13];
	}
	else
	{
		if(mr & MPU_READ_ACCE)
		{
			mpu_read_nbyte(MPU6500_ACCEL_XOUT_H, dat, 6);
			mpu_acce[0] = (dat[0] << 8) + dat[1];
			mpu_acce[1] = (dat[2] << 8) + dat[3];
			mpu_acce[2] = (dat[4] << 8) + dat[5];
		}
		if(mr & MPU_READ_TEMP)
		{
			mpu_read_nbyte(MPU6500_TEMP_OUT_H, dat, 2);
			mpu_temp = (dat[0] << 8) + dat[1];
		}
		if(mr & MPU_READ_GYRO)
		{
			mpu_read_nbyte(MPU6500_GYRO_XOUT_H, dat, 6);
			mpu_gyro[0] = (dat[0] << 8) + dat[1];
			mpu_gyro[1] = (dat[2] << 8) + dat[3];
			mpu_gyro[2] = (dat[4] << 8) + dat[5];
		}
		if(mr & MPU_READ_MAGN)
		{
			mpu_read_nbyte(MPU6500_EXT_SENS_DATA_00, dat, 7);
			if(dat[6] == 0x10)
			{
				mpu_magn[1] = (dat[1] << 8) + dat[0];
				mpu_magn[0] = (dat[3] << 8) + dat[2];
				mpu_magn[2] = (dat[5] << 8) + dat[4];
			}
		}
	}
}

void mpu_get(float *acc, float *gyr, float *mag, float *tem)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		acc[i] = (mpu_acce[i] + mpu_correction.acc_offset[i]) * mpu_correction.acc_gain[i];
		gyr[i] = (mpu_gyro[i] + mpu_correction.gyr_offset[i]) * mpu_correction.gyr_gain[i];
		mag[i] = (mpu_magn[i] + mpu_correction.mag_offset[i]) * mpu_correction.mag_gain[i];
	}
	*tem = MPU_TEMP_K * mpu_temp;
}

void mpu_get_acc(float *acc)
{
	int i;

	for(i = 0; i < 3; i++)
		acc[i] = (mpu_acce[i] + mpu_correction.acc_offset[i]) * mpu_correction.acc_gain[i];
#ifdef WEIYI
	sliding_filter_in(&acc_filter[0], acc[0]);
	sliding_filter_in(&acc_filter[1], acc[1]);
	sliding_filter_in(&acc_filter[2], acc[2]);
	if(states==0)
	{
	acc[0] = sliding_filter_out(&acc_filter[0]);
	acc[1] = sliding_filter_out(&acc_filter[1]);
	acc[2] = sliding_filter_out(&acc_filter[2]);
	}
	#endif
}

void mpu_get_gyr(float *gyr)
{
	int i;

	for(i = 0; i < 3; i++)
		gyr[i] = (mpu_gyro[i] + mpu_correction.gyr_offset[i]) * mpu_correction.gyr_gain[i];

}

void mpu_get_mag(float *mag)
{
	int i;
	float u;
	for(i = 0; i < 3; i++)
		mag[i] = (mpu_magn[i] + mpu_correction.mag_offset[i]) * mpu_correction.mag_gain[i];
	#ifdef WEIYI
	sliding_filter_in(&m_filter[0], mag[0]);
	sliding_filter_in(&m_filter[1], mag[1]);
	sliding_filter_in(&m_filter[2], mag[2]);
	if(states==0)
	{
	mag[0] = sliding_filter_out(&m_filter[0]);
	mag[1] = sliding_filter_out(&m_filter[1]);
	mag[2] = sliding_filter_out(&m_filter[2]);
	}

	#endif
	mag[2] = -mag[2];

}

unsigned char mpu_check()
{
	if(mpu_read_1byte(MPU6500_WHO_AM_I) == MPU6500_DEV_ID)
		return 0;

	return 1;
}

void mpu_init()
{
	int i;
	int reg_num;
	 print("\r\n check mpu9250  begin..\r\n");
	delay_ms(500);
		SPI_NCS_H();
		SPI_SCL_H();
		nop();
		SPI_SDI_H();
		nop();
		 print("\r\n check mpu9250  begin111..\r\n");
	while(mpu_check());
	mpu_init_correction();
	 print("\r\n check mpu9250  succeed..\r\n");
	reg_num = sizeof(MPU_INIT_REG) / 2;
	for(i = 0; i < reg_num; i++)
	{
		if(MPU_INIT_REG[i][0] != 0xFF)
			mpu_send_1byte(MPU_INIT_REG[i][0], MPU_INIT_REG[i][1]);
		else
			delay_ms(MPU_INIT_REG[i][1]);

		delay_ms(1);
	}

//	mpu_load_correction();
//	if(*(int *)&mpu_correction.acc_offset[0] == 0xFFFFFFFF
//		&& *(int *)&mpu_correction.acc_offset[1] == 0xFFFFFFFF
//		&& *(int *)&mpu_correction.acc_offset[2] == 0xFFFFFFFF)
//		mpu_para_init();
}


