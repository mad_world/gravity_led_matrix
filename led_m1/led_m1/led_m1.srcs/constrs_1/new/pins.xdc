set_property PACKAGE_PIN  W11  [get_ports {R1_S}]
set_property IOSTANDARD LVCMOS33 [get_ports {R1_S}]

set_property PACKAGE_PIN  V10  [get_ports {R2_S}]
set_property IOSTANDARD LVCMOS33 [get_ports {R2_S}]

set_property PACKAGE_PIN  W8  [get_ports {G1_S}]
set_property IOSTANDARD LVCMOS33 [get_ports {G1_S}]

set_property PACKAGE_PIN  AB11  [get_ports {G2_S}]
set_property IOSTANDARD LVCMOS33 [get_ports {G2_S}]

set_property PACKAGE_PIN  AB10  [get_ports {B1_S}]
set_property IOSTANDARD LVCMOS33 [get_ports {B1_S}]

set_property PACKAGE_PIN  AB9  [get_ports {B2_S}]
set_property IOSTANDARD LVCMOS33 [get_ports {B2_S}]

set_property PACKAGE_PIN  AA11  [get_ports {OE_S}]
set_property IOSTANDARD LVCMOS33 [get_ports {OE_S}]

set_property PACKAGE_PIN  Y11  [get_ports {SH_S}]
set_property IOSTANDARD LVCMOS33 [get_ports {SH_S}]


set_property PACKAGE_PIN  AA8   [get_ports {ST_S}]
set_property IOSTANDARD LVCMOS33 [get_ports {ST_S}]

set_property PACKAGE_PIN V12   [get_ports {ROW_Date_S[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ROW_Date_S[0]}]

set_property PACKAGE_PIN W10   [get_ports {ROW_Date_S[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ROW_Date_S[1]}]

set_property PACKAGE_PIN V9   [get_ports {ROW_Date_S[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ROW_Date_S[2]}]

set_property PACKAGE_PIN V8   [get_ports {ROW_Date_S[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ROW_Date_S[3]}]

set_property PACKAGE_PIN W12   [get_ports {ROW_Date_S[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ROW_Date_S[4]}]

set_property PACKAGE_PIN Y10   [get_ports {spi_clk_s_tri_o}]
set_property IOSTANDARD LVCMOS33 [get_ports {spi_clk_s_tri_o}]

set_property PACKAGE_PIN AA9   [get_ports {spi_mosi_s_tri_o}]
set_property IOSTANDARD LVCMOS33 [get_ports {spi_mosi_s_tri_o}]

set_property PACKAGE_PIN AB7   [get_ports {spi_miso_s_tri_i}]
set_property IOSTANDARD LVCMOS33 [get_ports {spi_miso_s_tri_i}]

set_property PACKAGE_PIN AB6   [get_ports {gpio_1_tri_o}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio_1_tri_o}]

