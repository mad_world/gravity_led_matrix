//Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2015.4 (win64) Build 1412921 Wed Nov 18 09:43:45 MST 2015
//Date        : Fri Jan 26 23:08:35 2018
//Host        : DESKTOP-8UROM78 running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (B1_S,
    B2_S,
    DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    G1_S,
    G2_S,
    OE_S,
    R1_S,
    R2_S,
    ROW_Date_S,
    SH_S,
    ST_S,
    gpio_1_tri_o,
    spi_clk_s_tri_o,
    spi_miso_s_tri_i,
    spi_mosi_s_tri_o);
  output B1_S;
  output B2_S;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  output G1_S;
  output G2_S;
  output OE_S;
  output R1_S;
  output R2_S;
  output [4:0]ROW_Date_S;
  output SH_S;
  output ST_S;
  output [0:0]gpio_1_tri_o;
  output [0:0]spi_clk_s_tri_o;
  input [0:0]spi_miso_s_tri_i;
  output [0:0]spi_mosi_s_tri_o;

  wire B1_S;
  wire B2_S;
  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire G1_S;
  wire G2_S;
  wire OE_S;
  wire R1_S;
  wire R2_S;
  wire [4:0]ROW_Date_S;
  wire SH_S;
  wire ST_S;
  wire [0:0]gpio_1_tri_o;
  wire [0:0]spi_clk_s_tri_o;
  wire [0:0]spi_miso_s_tri_i;
  wire [0:0]spi_mosi_s_tri_o;

  design_1 design_1_i
       (.B1_S(B1_S),
        .B2_S(B2_S),
        .DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .G1_S(G1_S),
        .G2_S(G2_S),
        .GPIO_1_tri_o(gpio_1_tri_o),
        .OE_S(OE_S),
        .R1_S(R1_S),
        .R2_S(R2_S),
        .ROW_Date_S(ROW_Date_S),
        .SH_S(SH_S),
        .ST_S(ST_S),
        .spi_clk_s_tri_o(spi_clk_s_tri_o),
        .spi_miso_s_tri_i(spi_miso_s_tri_i),
        .spi_mosi_s_tri_o(spi_mosi_s_tri_o));
endmodule
