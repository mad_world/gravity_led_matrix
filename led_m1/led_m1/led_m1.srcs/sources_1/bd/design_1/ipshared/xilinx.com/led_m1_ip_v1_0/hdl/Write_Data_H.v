`define DELAY_BIT  4 

module Write_Data_H ( 
                  
input             sys_clk             ,    //system clock;
input             sys_rst_n           ,    //system reset, low is active;
input start_s,
input [63:0]R1_Data_S,
input [63:0]G1_Data_S,
input [63:0]B1_Data_S,
input [63:0]R2_Data_S,
input [63:0]G2_Data_S,
input [63:0]B2_Data_S,


output R1_S,
output G1_S,
output B1_S,
output G2_S,
output R2_S,
output B2_S,
output DONE_S,
output IS_GOING_S,
output CLR_S
//,
//output reg [9:0]i
              );
			  
reg [63:0]R1_Data;
reg [63:0]G1_Data;
reg [63:0]B1_Data;
reg [63:0]R2_Data;
reg [63:0]G2_Data;
reg [63:0]B2_Data;
reg [4:0]ROW_Date;
reg IS_GOING;
reg start;
reg start1;
wire start_wire;

reg R1;
reg G1;
reg B1;
reg G2;
reg R2;
reg B2;
reg DONE;
reg CLR;
reg [31:0]count;
reg [9:0]i;
always @(posedge sys_clk or negedge sys_rst_n) begin 
        if (sys_rst_n ==1'b0) 
            count <= 32'b0;
			
        else if (( count == `DELAY_BIT ) ||(start_wire))
           begin
		    count <= 32'b0;
			end
		else 
            count <= count + 32'b1;
end
always @(posedge sys_clk or negedge sys_rst_n) begin 
        if (sys_rst_n ==1'b0) 
            begin
			start <= 1'b0;
			start1 <= 1'b0; 
			
			end
        else 
           begin
		  
			start<=start_s;
			start1<=start;
			end
		
end

 always @ ( posedge sys_clk or negedge sys_rst_n )
	     if( !sys_rst_n )
		      begin
					IS_GOING<=0;
					i <= 10'd0;
					DONE <= 1'b0;
					R1_Data<=64'b0;
					G1_Data<=64'b0;
					B1_Data<=64'b0;
					R2_Data<=64'b0;
					G2_Data<=64'b0;
					B2_Data<=64'b0;
					ROW_Date<=5'b0;
					R1<=1'b0;
                   R2<=1'b0;
                     G1<=1'b0;
                       G2<=1'b0;
                       B1<=1'b0;
                      B2<=1'b0;
                      CLR<=1'b0;
			   end
		 else if(start_wire)
			begin
			i<=0;
			IS_GOING<=1;
			end
		 else	if( count == `DELAY_BIT ) 
			begin
				if(i==129)
					begin
						DONE<=1;
						i<=i+1;
					end
				else if(i==130)
					begin
						DONE<=0;
						IS_GOING<=0;
					end
				else if(i==0)
					begin
						R1_Data<=R1_Data_S;
						G1_Data<=G1_Data_S;
						B1_Data<=B1_Data_S;
						R2_Data<=R2_Data_S;
						G2_Data<=G2_Data_S;
						B2_Data<=B2_Data_S;
						i<=i+1;
						DONE<=0;
					end
				else if(i[0]==1)
					begin
						R1<=(R1_Data[(i-1)>>1]);
						R2<=(R2_Data[(i-1)>>1]);
						G1<=(G1_Data[(i-1)>>1]);
						G2<=(G2_Data[(i-1)>>1]);
						B1<=(B1_Data[(i-1)>>1]);
						B2<=(B2_Data[(i-1)>>1]);
						CLR<=1'b0;
						i<=i+1;
					end
				else
					begin
						CLR<=1'b1;
						i<=i+1;
					end
			end
		

assign start_wire=(~start)&start1;
assign ROW_OUT_S=ROW_Date;
assign R1_S=R1;
assign G1_S=G1;
assign B1_S=B1;
assign G2_S=G2;
assign R2_S=R2;
assign B2_S=B2;
assign DONE_S=DONE;
assign CLR_S=CLR;
assign IS_GOING_S=IS_GOING;
endmodule

