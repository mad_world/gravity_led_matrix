module Write_Data_32 (                
input             sys_clk             ,    //system clock;
input             sys_rst_n           ,    //system reset, low is active;

output [4:0]ROW_Date_S,
output R1_S,
output G1_S,
output B1_S,
output G2_S,
output R2_S,
output B2_S,
output ST_S,
output SH_S,
input start_zhen,//
output OE_S,

input [11:0]address,
input [2:0]color,
input write

              );
			  

 reg [2:0]data[64*64-1:0];
/* (* ramstyle = "no_rw_check , m4k" , ram_init_file = "Write_Data_32.mif" *) reg [63:0] r_data[63:0];
(* ramstyle = "no_rw_check , m4k" , ram_init_file = "Write_Data_32.mif" *) reg [63:0] g_data[63:0];
(* r amstyle = "no_rw_check , m4k" , ram_init_file = "Write_Data_32.mif" *) reg [63:0] b_data[63:0];*/

wire DONE;

wire IS_GOING_S;
reg ST;
reg start;

reg [63:0]R1_Data_M[31:0];
reg [63:0]G1_Data_M[31:0];
reg [63:0]B1_Data_M[31:0];
reg [63:0]R2_Data_M[31:0];
reg [63:0]G2_Data_M[31:0];
reg [63:0]B2_Data_M[31:0];


reg [63:0]R1_Data;
reg [63:0]G1_Data;
reg [63:0]B1_Data;
reg [63:0]R2_Data;
reg [63:0]G2_Data;
reg [63:0]B2_Data;
reg [4:0]ROW_Date;
reg [31:0]count;
reg OE;
reg done1;
reg done2;
reg setted;
wire done_wire;
wire [63:0]R1_Data_S;
wire [2:0]RS74;
wire [31:0]COUNT_S;
	assign COUNT_S=count;	
	assign 	  RS74=data[74];
always @ ( posedge sys_clk or negedge sys_rst_n )
 begin
 if (sys_rst_n ==1'b0) 
		 begin
			done2<=1'b0;
			done1<=1'b0;
			 for(k=3;k<10*64;k=k+1) data[k]<=3'd7;
                         for(k=10*64;k<64*64;k=k+1) data[k]<=3'd1;
                       data[0]<=3'd7;
                       data[2]<=3'd7;
                       data[1]<=3'd7;
		 end
	else begin
	done2<=done1;
	done1<=DONE_S;
	if(write)
	begin
	   data[address]<=color;
	end
	end
end 

 integer i,k,j;
 
 always @ ( posedge sys_clk or negedge sys_rst_n )
 begin	
	if (sys_rst_n ==1'b0) 
		 begin
		      OE<=1;
			count<=0;
			R1_Data<=64'd0;
			G1_Data<=64'd0;
			B1_Data<=64'd0;
			R2_Data<=64'd0;
			G2_Data<=64'd0;
			B2_Data<=64'd0;
			ROW_Date<=5'd0;
			start<=0;
//			for(k=0;k<64*64;k=k+1)
//			begin
//			n=k/64;
//			m=k%64;
//			 if(m-64)
//			end
			
			ST<=0;
			setted<=0;
		 end
	else if(start_zhen)
	begin
		if(count==0)
		begin
					for(j=0;j<32;j=j+1)
					begin
					for(i=0;i<64;i=i+1)
					 begin
					
                        R1_Data_M[j][i]<=data[j*64+i][0];
                        R2_Data_M[j][i]<=data[j*64+i+32*64][0];
                        G1_Data_M[j][i]<=data[j*64+i][1];
                        G2_Data_M[j][i]<=data[j*64+i+32*64][1];
                        B1_Data_M[j][i]<=data[j*64+i][2];
                        B2_Data_M[j][i]<=data[j*64+i+32*64][2];
					 end
					 end
					 count<=count+1;
		end
		else if(count<4097*4)
		begin//32*4
		case(count[8:0])
			1:
			begin
			         if(~setted)
			         begin
					  // for(i=0;i<64;i=i+1)
					 // begin
                        // R1_Data[i]<=data[(count>>2)<<6+i][0];
                        // R2_Data[i]<=data[(count>>2)<<6+i+32<<6][0];
                        // G1_Data[i]<=data[(count>>2)<<6+i][1];
                        // G2_Data[i]<=data[(count>>2)<<6+i+32<<6][1];
                        // B1_Data[i]<=data[(count>>2)<<6+i][2];
                        // B2_Data[i]<=data[(count>>2)<<6+i+32<<6][2];
					 // end
						R1_Data<=R1_Data_M[count>>9];
                        R2_Data<=R2_Data_M[count>>9];
                        G1_Data<=G1_Data_M[count>>9];
                        G2_Data<=G2_Data_M[count>>9];
                        B1_Data<=B1_Data_M[count>>9];
                        B2_Data<=B2_Data_M[count>>9];
						setted<=1;
						OE<=1;
					 end
					 else
					 begin
					
					count<=count+1;
					
					start<=1;
					setted<=0;
					end
			end
			
			2:
			begin
				start<=0;
				count<=count+1;
			end
			3:
			begin 
				if(done_wire)
				        begin
						count<=count+1;
		                  ST<=0;
		                  end	
			end
			4:
                        begin 
						
                          ST<=1;  
                          count<=count+1;
                        end
             5:
                         begin 
                                    ROW_Date<=(count>>9); 
                                    count<=count+1;
                            end          
          100:
                     begin 
					 OE<=0;
					 
                    count<=count+1;
                   end
		default: count<=count+1; 
		endcase;
		end
	else 
		begin
					count<=0;
		end
		
		
//			if(count==130)
//				begin
//					count<=count+1;
//					ST<=1;
//				end
//			else if(count==131)
//				begin
//					count<=0;
//					ST<=0;
//				end
//			else if(count==129)
//				count<=count+1;
//			else if((~count[0])&(~count[1]))
//				begin
//					R1_Data<=r_data[count>>2];
//					G1_Data<=g_data[count>>2];
//					B1_Data<=b_data[count>>2];
//					R2_Data<=r_data[count>>2+32];
//					G2_Data<=g_data[count>>2+32];
//					B2_Data<=b_data[count>>2+32];
//					count<=count+1;
//					ROW_Date<=(count>>2);
//					start<=1;
//				end
//			else if((count[0])&(~count[1]))
//				begin
//					start<=0;
//					count<=count+1;
//				end
//			else if((count[0])&(count[1]))
//				begin
//					if(~IS_GOING_S)
//						count<=count+1;
//				end
//			else
//				count<=count+1;
	end 
 
 
 end
 Write_Data_H Write_Data_H1 ( 
                  
. sys_clk  (sys_clk)           ,    //system clock;
. sys_rst_n  (sys_rst_n)    ,   //system reset, low is active;
.start_s(start),
.R1_Data_S(R1_Data),
.G1_Data_S(G1_Data),
.B1_Data_S(B1_Data),
.R2_Data_S(R2_Data),
.G2_Data_S(G2_Data),
.B2_Data_S(B2_Data),


. R1_S(R1_S),
.G1_S(G1_S),
.B1_S(B1_S),
.G2_S(G2_S),
.R2_S(R2_S),
.B2_S(B2_S),
.DONE_S(DONE_S),
.IS_GOING_S(IS_GOING_S),
. CLR_S(SH_S)

              );
//assign 	COUNT_S=DONE_S; 	
assign done_wire=(~done1)&done2;
assign ROW_Date_S=ROW_Date;
assign ST_S=ST;
assign OE_S=OE;


endmodule

